# vim:ft=kitty
# General Settings
confirm_os_window_close 0
font_family      RobotoMono Nerd Font Mono Medium
bold_font        RobotoMono Nerd Font Mono Bold
italic_font      RobotoMono Nerd Font Mono Medium Italic
bold_italic_font RobotoMono Nerd Font Mono Bold Italic
font_size 9
window_margin_width 4
background_opacity 1.0
# The basic colors
foreground              #CDD6F4
background              #1e1e2e
selection_foreground    #1E1E2E
selection_background    #F5E0DC

# Cursor colors
cursor                  #F5E0DC
cursor_text_color       #1E1E2E

# URL underline color when hovering with mouse
url_color               #F5E0DC

# Kitty window border colors
active_border_color     #74C7EC
inactive_border_color   #45475A
bell_border_color       #F9E2AF

# OS Window titlebar colors
wayland_titlebar_color system
macos_titlebar_color system

# Tab bar colors
active_tab_foreground   #11111B
active_tab_background   #74C7EC
inactive_tab_foreground #CDD6F4
inactive_tab_background #11111B
tab_bar_background      #1E1E2E

# Colors for marks (marked text in the terminal)
mark1_foreground #1E1E2E
mark1_background #B4BEFE
mark2_foreground #1E1E2E
mark2_background #CBA6F7
mark3_foreground #1E1E2E
mark3_background #74C7EC

# The 16 terminal colors

color0 #11111B
color8 #313244
#: black

color1 #F38BA8
color9 #F5C2E7
#: red

color2  #A6E3A1
color10 #94E2D5
#: green

color11 #FAB387
color3  #F9E2AF
#: yellow

color4  #89B4FA
color12 #74C7EC
#: blue

color5  #CBA6F7
color13 #B4BEFE
#: magenta

color6  #89DCEB
color14 #89DCEB
#: cyan

color7  #CDD6F4
color15 #9399B2
#: white

tab_separator "  | "
tab_separator_color #ff00ff
tab_bar_min_tabs            2
tab_bar_edge                bottom
tab_bar_style               powerline
tab_powerline_style         angled
tab_title_template          {title}{' :{}:'.format(num_windows) if num_windows > 1 else ''}
