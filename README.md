
* https://github.com/catppuccin/catppuccin
* The window title script/module for Polybar is from https://github.com/nekowinston/i3-window-title you'll need to put the executable in your $PATH.
* Btop is just the theme from Catppuccin.
* VIM is just Airline + Syntax themes from Catppuccin.
* I left comments in some of the config files for specific things.
